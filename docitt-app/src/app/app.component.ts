import { Component } from '@angular/core';
import { RoutingServices } from "app/common-services/routingServices.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[RoutingServices]
})
export class AppComponent {
    constructor(private routingService:RoutingServices) {
  }
  goHome() {
    this.routingService.navigatePageUrl('');
}
}