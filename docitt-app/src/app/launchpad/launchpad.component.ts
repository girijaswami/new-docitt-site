import { Component, OnInit } from '@angular/core';
import { AssetsModelTransporterService } from "app/launchpad/assets/assets-model-transporter.service";

@Component({
  selector: 'app-launchpad',
  templateUrl: './launchpad.component.html',
  styleUrls: ['./launchpad.component.scss'],
  providers:[AssetsModelTransporterService]
})
export class LaunchpadComponent implements OnInit {
  constructor() { }

//This is to add the side navs where each object represents an object which has key as navName and value as routing URL.
  sideNavList = [
    {navName:"Profile",URL:"profile"},
    {navName:"Assets",URL:"assets"},
    {navName:"Income",URL:"income"},
    {navName:"Declaration",URL:"declaration"},
    {navName:"Credit",URL:"credit"},
    {navName:"Summary",URL:"summary"}];
  ngOnInit() {}

}
