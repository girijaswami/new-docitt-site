import { RequestOptions } from '@angular/http';
import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { RoutingServices } from "app/common-services/routingServices.service";
import { HttpServices } from 'app/common-services/httpServices.service';
import { AssetsModelTransporterService } from 'app/launchpad/assets/assets-model-transporter.service';

@Component({
  selector: 'app-assets-1',
  templateUrl: './assets-1.component.html',
  styleUrls: ['./assets-1.component.scss']
})
export class Assets1Component implements OnInit {
  isClick = false;
  bankId = '';
  bankLogo = '';
  bankName = '';
  bankList: any[];
  bankToSearch = '';
  bankSearchResultsList: any[];
  bankSearchResults = false;

  constructor(private http:Http,private routingService: RoutingServices, private httpService: HttpServices, private assetsTransport: AssetsModelTransporterService) { }

  ngOnInit() {
    let bankImagesUrl: string = 'https://sandbox.docitt.net/api/institutions/Favourites';
    this.httpService.getData(bankImagesUrl).subscribe(
      (response: any) => {
        const banks = response.json();
        for (const bank of banks) {
          bank.logo = "data:image/png;base64," + bank.logo;
        }
        this.bankList = banks;
      }
    );
  }
  searchBank(){
    let searchBankUrl: string = ' https://sandbox.docitt.net/api/institutions/Search ';
    let body = JSON.stringify(this.bankToSearch);

    this.httpService.postData(searchBankUrl,body).subscribe(
      (response: any) => {
        const banks = response;
        for (const bank of banks) {
          bank.logo = "data:image/png;base64," + bank.logo;
        }
       this.bankSearchResultsList = banks;
       this.bankSearchResults = true;
        console.log(banks);
      }
    );
  }
  loadNext() {
    this.assetsTransport.bankDetails.bankId = this.bankId;
    this.assetsTransport.bankDetails.bankName = this.bankName;
    this.assetsTransport.bankDetails.imgUrl = this.bankLogo;
    // this.assetsTransport.transportImage(this.assetsTransport.bankDetails);
    this.routingService.navigatePageUrl('launchpad/assets2');

  }
}
