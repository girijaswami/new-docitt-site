import { ActivatedRoute } from '@angular/router';
import { forwardRef, Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { RoutingServices } from "app/common-services/routingServices.service";
import { Assets2Service } from "app/launchpad/assets/assets-2/assets-2.service";
import { AssetsModelTransporterService } from "app/launchpad/assets/assets-model-transporter.service";
import {Http} from '@angular/http' 

@Component({
  selector: 'app-assets-2',
  templateUrl: './assets-2.component.html',
  styleUrls: ['./assets-2.component.scss'],
  providers: [Assets2Service]
})
export class Assets2Component implements OnInit {
  getData: any;
  imageSource: string;
  instituitionId: any;
  private resBody: any;
  private challengeResponse: any;
  user = '';
  pass = '';
  invitationId = '';
  providerId = '';
  private baseURL: string = 'https://sandbox.docitt.net/api/customer/';
  documentsList={};
  dowloadDocuments=false;
  reqBody = {
    "CustomerId": "",
    "InvitationId": "",
    "InstitutionId": "",
    "Secert": this.pass,
    "UserName": this.user
  };
  responseBody = {
    data: "",
    flag: false,
    challengeId: ""
  };
  bankImage: { id: string, logo: string, name: string }
  constructor(private assetsTransporter: AssetsModelTransporterService, private assetsService: Assets2Service, private routing: RoutingServices, private route: ActivatedRoute,private http:Http) {
    let bankImageModel: any;
    bankImageModel = this.assetsTransporter.bankDetails;
    this.instituitionId = this.assetsTransporter.bankDetails.bankId;

  }
  goBack() {
    this.routing.navigatePageUrl('launchpad/assets');
  }
  ngOnInit() {
    this.imageSource = this.assetsTransporter.bankDetails.imgUrl;
    
  }

  authenticate(f) {
    this.user = f.value.onlineId;
    this.pass = f.value.password;
    this.reqBody.InvitationId = f.value.transaction;
    this.reqBody.Secert = this.pass;
    this.reqBody.UserName = this.user;
    let url2: string = "https://sandbox.docitt.net/api/company/5923053ac3310c4330444adf/invitation/" + f.value.transaction;
    this.assetsService.getCall(url2).subscribe((response: Response) => {
      let url: string = this.baseURL + response.json().customerId + '/institution/' + this.instituitionId + '';
      this.reqBody.CustomerId = response.json().customerId;
      this.reqBody.InstitutionId = this.instituitionId;
      this.assetsTransporter.custData = this.reqBody;
      this.resBody = this.assetsService.authenticateUser(url, this.reqBody);
    },
      (error) => {
      })
  }

  getChallenges() {
    this.getInstitution();
    this.getCustomerDocuments(this.reqBody.CustomerId);
    let url: string = this.baseURL +  + '/account/1/challenge';
    this.challengeResponse = this.assetsService.getCall(url).subscribe(
      (response: Response) => {
        this.responseBody.data = "Enter the code:";
        this.responseBody.flag = true;
        this.responseBody.challengeId = response.json()[0].challengeId;
        return this.responseBody;
      }
    );
  }

  getInstitution() {
    let url: string = this.baseURL + this.reqBody.CustomerId + '/institution';
    this.assetsService.getCall(url).subscribe(
      (response: Response) => {
        let data = response.json()[0].providerId;
        this.providerId = data;
      }
    );
  }

  submitChallenge(cust) {

    let body = {
      "ChallengeId": this.responseBody.challengeId,
      "CustomerId": this.reqBody.CustomerId,
      "InstitutionId": this.reqBody.InstitutionId,
      "InvitationId": this.reqBody.InvitationId,
      "ChallengeType": "Question",

      // "mfa":["1234"]

      //  "Response":{
      //   "mfa":["1234"]
      //  },
      "ProviderId": this.providerId
    }

    let url = this.baseURL + this.reqBody.CustomerId + '/institution/challenge';
    let result = this.assetsService.postOtp(url, body);


  }
  getCustomerDocuments(customerId){
    console.log("Getting documntes ID : >>>>");
   
    //URL hit to get the Documents List
    //https://sandbox.docitt.net/api/customer/{{customerid}}/document 
    //let url = this.baseURL + this.reqBody.CustomerId + 'document';
    let url = this.baseURL + customerId + '/document';
    this.assetsService.getCall(url).subscribe(
      (response: Response) => {
        
       this.documentsList= response.json();
       this.dowloadDocuments=true;
      
      }
    );
  }
 onDownload(documentId){
     let url = this.baseURL+this.reqBody.CustomerId+'/document/'+documentId+'/download?roleId=1';
     this.http.get(url).subscribe(
       (response:Response)=>{
        
         this.downloadFile(response);
         
     },
       (error)=>{console.log(">>>Error "+error)}
     );
   }
  downloadFile(data: Response){
  var blob = new Blob([data], { type: 'text/csv' });
  var url= window.URL.createObjectURL(blob);
  window.open(url);
}
  getSummary(cust) {
    let url: string = this.baseURL + cust + '/account/Summary';
    this.assetsTransporter.custData = this.reqBody;
    this.challengeResponse = this.assetsService.getCall(url).subscribe(
      (response: any) => {
        this.assetsTransporter.accountList = [];
        this.assetsTransporter.accountList = response.json();
        this.assetsTransporter.custId = cust;
        this.routing.navigatePageUrl('launchpad/assets3');
      }
    )
  }
  type = "password";
  changeVisual() {
    if (this.type === "password")
      this.type = "text";
    else
      this.type = "password";
  }
  OnSubmit(f) {
    console.log("Summit button isclicked");
    console.log();

  }
}
