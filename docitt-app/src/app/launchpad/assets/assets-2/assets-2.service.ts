
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { HttpServices } from "app/common-services/httpServices.service";



@Injectable()
export class Assets2Service {
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers });
    constructor(private http: Http, private httpService: HttpServices) { }
    authenticateUser(url, body) {
        let returnResponse = {
            data: "processing your request",
            flag: false,
            errorFlag: false,
            custId: ""
        };
        let bodyData = body;
        this.httpService.postData(url, body).subscribe(
            (data) => {
                returnResponse.flag = true;
                returnResponse.custId = body.CustomerId;
            },
            (error) => {
                returnResponse.data = error._body;
                returnResponse.errorFlag = true;
            }
        )
        return returnResponse;
    };
    getCall(url) {
        return this.http.get(url, this.options);
    };

    postOtp(url, body){
        console.log("Inside the post OTP ");
           this.httpService.postData(url, body).subscribe(
            (data) => {
              console.log("Data >>> "+data);
              return data;
            },
            (error) => {
                console.log(" <<>>> "+error);
                return error;
               
            }
        )

    }

}