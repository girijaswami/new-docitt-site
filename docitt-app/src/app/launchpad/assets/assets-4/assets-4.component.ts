import { Pipe, PipeTransform } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AssetsModelTransporterService } from "app/launchpad/assets/assets-model-transporter.service";
import { HttpServices } from "app/common-services/httpServices.service";

@Component({
  selector: 'app-assets-4',
  templateUrl: './assets-4.component.html',
  styleUrls: ['./assets-4.component.scss']
})
export class Assets4Component implements OnInit {
  accountFlag: boolean;
  accountSelected: any;
  accountType: any;
  errorMsg: any;
  zeroFlag = false;
  errorFlag = false;
  typeFlag = false;
  filterList: any;
  detailsFlag = false;
  typeSelected;
  btn1='';
  btn2='';
  btn3='';
  accountDetails:any;
  accountData={
    accNumber : "",
    accType :"",
    transactionId : "",
    date : "",
    type : "",
    amt :"",
    flags : ""
  };
  accountList = [];

  
  constructor(private assetTransporter: AssetsModelTransporterService,private httpService:HttpServices) {
    this.filterList = assetTransporter.viewDataFilter;
    console.log(this.filterList);
  }
  onSelectionChange(e){
    switch (e) {
      case 0:
        this.btn1 = "active";
        this.btn2 = '';
        this.btn3 = '';
        break;
        case 1:
         this.btn1 = '';
        this.btn2 = "active";
        this.btn3 = '';
        break;
        case 2:
        this.btn1 = '';
        this.btn2 = '';
        this.btn3 = "active";
        break;
    
      default:
        break;
    }
    this.typeSelected = e;
    this.typeFlag = true;

  }
  onSelectionChangeAccount(e){
    this.accountSelected = e;
    this.accountFlag = true;
  }

transform(value: string) {
       var datePipe = new DatePipe("en-US");
        value = datePipe.transform(value, 'MM-dd-yyyy');
        return value;
    }
filterData(f){
  console.log(f);
  this.accountType = this.assetTransporter.viewDataFilter;
  f.value.fromDate = this.transform(f.value.fromDate);
  f.value.toDate = this.transform(f.value.toDate);
  let url = "https://sandbox.docitt.net/api/customer/"+this.assetTransporter.custId+"/account/FilteredData/"+f.value.selectedValue+"/"+f.value.fromDate+"/"+f.value.toDate+"/"+this.typeSelected;
  this.httpService.getData(url).subscribe(
    (response)=>{
      this.detailsFlag = true;
      this.accountDetails = response.json().transactions;
      if(this.accountDetails.length == 0 ){
        this.zeroFlag = true;
      }
      //  else {
      //     for(let element of this.accountDetails) {
      //       for(let element2 of element){
      //     let accountItem = {};
      //     this.accountData.accNumber = element.accountNumber;
      //     this.accountData.accType = element.accountType;
      //     this.accountData.transactionId = element2.transactionId;
      //     this.accountData.date = element2.transactionDate;
      //     this.accountData.type = element2.type;
      //     this.accountData.amt = element2.amount;
      //     this.accountData.flags = element2.invitationFlags?element2.invitationFlags.length:"-";
      //     accountItem = this.accountData;
      //     this.accountList.push(accountItem);
      //       }           
        //  this.accountDetails.forEach(element => {
        //    let accountItem = {};
        // element.transactions.forEach(element2 => {
        //   // let accountItem = {};
        //   this.accountData.accNumber = element.accountNumber;
        //   this.accountData.accType = element.accountType;
        //   this.accountData.transactionId = element2.transactionId;
        //   this.accountData.date = element2.transactionDate;
        //   this.accountData.type = element2.type;
        //   this.accountData.amt = element2.amount;
        //   this.accountData.flags = element2.invitationFlags?element2.invitationFlags.length:"-";
        //   accountItem = this.accountData;
        //   console.log(accountItem);
        //   this.accountList.push(accountItem);
      //   });
      // });
      // console.log(this.accountList);
      },(error)=>{
      this.errorFlag = true;
      this.errorMsg = error._body;
    }
    )
}
  ngOnInit() {
  }

}
