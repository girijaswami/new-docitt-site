import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Assets4Component } from './assets-4.component';

describe('Assets4Component', () => {
  let component: Assets4Component;
  let fixture: ComponentFixture<Assets4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Assets4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Assets4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
