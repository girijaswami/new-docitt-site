import { Component, OnInit } from '@angular/core';
import { RoutingServices } from "app/common-services/routingServices.service";

@Component({
  selector: 'app-launch-home',
  templateUrl: './launch-home.component.html',
  styleUrls: ['./launch-home.component.scss'],
   providers:[RoutingServices]
})
export class LaunchHomeComponent implements OnInit {

  constructor(private routingService:RoutingServices) { }
loadNext(){
  this.routingService.navigatePageUrl('launchpad/profile');
}
  ngOnInit() {
  }

}
