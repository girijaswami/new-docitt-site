import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RoutingServices } from "app/common-services/routingServices.service";
import { NgForm } from '@angular/forms'
import { ProfileOneService } from "./profile-1.service";
@Component({
  selector: 'app-profile-1',
  templateUrl: './profile-1.component.html',
  styleUrls: ['./profile-1.component.scss'],
})
export class Profile1Component implements OnInit {

  @ViewChild('f') accountForm: NgForm;

  phoneCliked = false;
  emailCliked = false;
  textCliked = false;
  allCliked = false;
  userExist = false;
  onOptionClick(event) {
    var buttonName = event.target.innerText;

    switch (buttonName) {
      case 'Phone':
        this.phoneCliked = !this.phoneCliked;
        break;
      case 'Email':
        this.emailCliked = !this.emailCliked;
        break;
      case 'Text':
        this.textCliked = !this.textCliked;
        break;
      case 'All':
        this.allCliked = !this.allCliked;
        if (this.allCliked) {
          this.phoneCliked = true;
          this.emailCliked = true;
          this.textCliked = true;
        } else {
          this.phoneCliked = false;
          this.emailCliked = false;
          this.textCliked = false;
        }
        break;
    }

    if (this.emailCliked && this.textCliked && this.phoneCliked)
      this.allCliked = true;
    else
      this.allCliked = false;
  }

  constructor(private routingService: RoutingServices, private profileService: ProfileOneService) { }
  loadNext() {
    this.routingService.navigatePageUrl('launchpad/profile2');
  }
  loadPrevious() {
    console.log("Previous buttoon is cliked");
  }
  ngOnInit() {
  }
  onSubmit() {

    var user =
      {
        "email": this.accountForm.value.email,
        "firstName": this.accountForm.value.firstName,
        "lastName": this.accountForm.value.lastName,
        "PhoneNumber": this.accountForm.value.phone
      };

    this.profileService.createUser(user).subscribe(

      (response: any) => {
        sessionStorage.setItem("userID", response.customerId);
        this.routingService.navigatePageUrl('launchpad/profile2');
        
      },
      (error) => {
        this.userExist = true;
      }
    );

  }
}
