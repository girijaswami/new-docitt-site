import {Injectable} from '@angular/core'
import {Http,Response} from '@angular/http'
import 'rxjs/RX'
import {Observable} from 'rxjs/Observable'
@Injectable()
export class ProfileOneService{

constructor(private http:Http){}
    createUser(user){
        
        return this.http.post('https://sandbox.docitt.net/api/customer/',user).
        map(
            (response:Response)=>{
                return response.json();
            }
        ).catch(
            (error:Response)=>{
                return Observable.throw(error);
            }
        );
       
        
    }

}