import { Component, OnInit } from '@angular/core';
import { RoutingServices } from "app/common-services/routingServices.service";
@Component({
  selector: 'app-profile-2',
  templateUrl: './profile-2.component.html',
  styleUrls: ['./profile-2.component.scss']
})
export class Profile2Component implements OnInit {

goBack(){
  this.routingService.navigatePageUrl('launchpad/profile');
}

  constructor(private routingService:RoutingServices) { }
loadNext(){
  this.routingService.navigatePageUrl('launchpad/profile3');
}
  ngOnInit() {
  }

}
