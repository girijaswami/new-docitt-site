//Angular imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//App imports
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

//launchpad imports
import { LaunchpadComponent } from './launchpad/launchpad.component';
import { ProgressIndicatorComponent } from './launchpad/progress-indicator/progress-indicator.component';
import { LaunchHomeComponent } from './launchpad/launch-home/launch-home.component';
import { Profile1Component } from './launchpad/profile/profile-1/profile-1.component';
import { Profile2Component } from './launchpad/profile/profile-2/profile-2.component';
import { Profile3Component } from './launchpad/profile/profile-3/profile-3.component';
import { Assets1Component } from './launchpad/assets/assets-1/assets-1.component';
import { Assets2Component } from './launchpad/assets/assets-2/assets-2.component';
import { Assets3Component } from './launchpad/assets/assets-3/assets-3.component';

//Routers imports
import { AppRoutingModule } from "app/routes/app-routing.module";
import { LaunchpadProfileModule } from "app/routes/launchpad-profile.module";
import { LaunchpadAssetsModule } from "app/routes/launchpad-assets.module";

import { ProfileOneService } from "app/launchpad/profile/profile-1/profile-1.service";
import { HttpServices } from "app/common-services/httpServices.service";
import { RoutingServices } from "app/common-services/routingServices.service";
import { Assets4Component } from './launchpad/assets/assets-4/assets-4.component';
@NgModule({
  declarations: [
    AppComponent,
    LaunchpadComponent,
    ProgressIndicatorComponent,
    Profile1Component,
    Profile2Component,
    Profile3Component,
    LaunchHomeComponent,
    HomeComponent,
    Assets1Component,
    Assets2Component,
    Assets3Component,
    LoginComponent,
    Assets4Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    LaunchpadAssetsModule,
    LaunchpadProfileModule
    ],
  providers: [HttpServices,RoutingServices,ProfileOneService],
  bootstrap: [AppComponent]
})
export class AppModule { }
