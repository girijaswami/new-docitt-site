import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LaunchpadComponent } from "app/launchpad/launchpad.component";
import { Assets1Component } from "app/launchpad/assets/assets-1/assets-1.component";
import { Assets2Component } from "app/launchpad/assets/assets-2/assets-2.component";
import { Assets3Component } from "app/launchpad/assets/assets-3/assets-3.component";
import { Assets4Component } from "app/launchpad/assets/assets-4/assets-4.component";

const assetsRoutes: Routes = [
    {
        path: 'launchpad', component: LaunchpadComponent, children: [
            { path: 'assets', component: Assets1Component },
            { path: 'assets2', component: Assets2Component },
            { path: 'assets3', component: Assets3Component },
            { path: 'assets4', component: Assets4Component }

        ]
    }
];
@NgModule({
    imports: [RouterModule.forRoot(assetsRoutes)],
    exports: [RouterModule]
})
export class LaunchpadAssetsModule {

}