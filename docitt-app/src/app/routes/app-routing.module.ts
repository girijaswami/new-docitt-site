import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import { HomeComponent } from "app/home/home.component";
import { LoginComponent } from "app/login/login.component";
import { LaunchpadComponent } from "app/launchpad/launchpad.component";
import { LaunchHomeComponent } from "app/launchpad/launch-home/launch-home.component";

const appRoutes: Routes = [
  {path:'',component: HomeComponent},
  {path:'login',component: LoginComponent},
  {path:'launchpad',component: LaunchpadComponent,children:[
    {path:'',component: LaunchHomeComponent }
  ]}
  ];
@NgModule({
imports:[RouterModule.forRoot(appRoutes)],
exports:[RouterModule]
})
export class AppRoutingModule{

}