import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { LaunchpadComponent } from "app/launchpad/launchpad.component";
import { LaunchHomeComponent } from "app/launchpad/launch-home/launch-home.component";
import { Profile1Component } from "app/launchpad/profile/profile-1/profile-1.component";
import { Profile2Component } from "app/launchpad/profile/profile-2/profile-2.component";
import { Profile3Component } from "app/launchpad/profile/profile-3/profile-3.component";

const profileRoutes:Routes =[
    {path:'launchpad',component: LaunchpadComponent,children:[
    {path:'profile',component: Profile1Component },
    {path:'profile2',component: Profile2Component },
    {path:'profile3',component: Profile3Component },
]}
];
@NgModule({
imports:[RouterModule.forRoot(profileRoutes)],
exports:[RouterModule]
})
export class LaunchpadProfileModule{

}