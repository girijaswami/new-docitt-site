import { Injectable } from "@angular/core";
import{Http,Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/RX';
import { Observable } from "rxjs/Observable";

const headers = new Headers({'Content-Type': 'application/json'});
const options = new RequestOptions({headers:headers});

@Injectable()
export class HttpServices{
    constructor(private http:Http){}
    
    getData(url:string){
        return this.http.get(url,options);
    }
    
    postData(url:string,body:any){
        return this.http.post(url,body,options).map(
            (response:Response) =>{
                const data = response.json();
                return data;
            }
        );
    }
}